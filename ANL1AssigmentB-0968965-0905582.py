# imports
import turtle
import time
import sys

# canvas settings
app = turtle
app.setup(800, 800)
app.bgcolor('#113f89')
app.speed('fastest')
app.hideturtle()
app.tracer(0)

# game settings
names	= False
turn 	= 0

# heart of the game
def make_game():
	heart = []
	for col in range(7):
		heart.append([])
		for row in range(6):
			heart[col].append(0)
	return heart

game = make_game()

# player data
class Player:
	def __init__(self, name='', move=0, **kwargs):
		self.name = name
		self.move = move

# checks if name input is valid
def get_name(player):
	while True:
		name = input('What is the name of ' + player + '? \n > ')
		if len(name) == 0:
			print('You have not inserted a name.')
		elif len(name) < 2:
			print('A name should at least be 2 characters long.')
		elif len(name) > 10:
			print('Please choose a smaller name.')
		else:
			name = name.capitalize()
			return name
			break

# draw and update the board game
def draw_board(change):
	app.penup()
	app.setheading(90)
	app.goto(-275,-300)
	app.pendown()
	start_pos_y = -170

	for row in range(7):
		for col in range(6):
			app.color('#FFFFFF')
			if change[row][col] == 1:
				app.fillcolor('#f2e132')
			elif change[row][col] == 2:
				app.fillcolor('red')
			else:
				app.fillcolor('#FFFFFF')

			if change[row][col] == 1 or 2:
				app.begin_fill()
				app.circle(50)
				app.end_fill()

			app.penup()
			app.forward(105)
			app.pendown()

		app.penup()
		app.forward(-100)
		app.goto(start_pos_y,-300)
		app.pendown()
		start_pos_y += 105

	app.penup()
	app.goto(-275,-300)
	app.pendown()
	app.update()

def draw_column_numbers():
	app.penup()
	app.goto(-338, 0)
	app.forward(290)
	app.right(90)

	for number in range(1, 8):
		app.write(number, font=('Arial', 40, 'bold'))
		app.forward(106)

def row_full(player_move):
	counter = 0
	for col in game[player_move -1]:
		if col > 0:
			counter += 1
	return counter

def message(msg):
	print('\n'+('__'*40)+'\n')

	for i in msg:
		sys.stdout.write(i)
		sys.stdout.flush()
		time.sleep(0.02)

	print('\n' + '__'*40+'\n')

def check_win_vertical(player):
	counter = 0
	for x in range(0,7):
		for y in game[x]:
			if y == player:
				counter += 1
			else:
				counter = 0
			if counter == 4:
				return True
		counter = 0
	return False

def check_win_horizontal(player):
	counter = 0
	for row in range(0,6):
		for col in range(0,7):
			if game[col][row] == player:
				counter += 1
			else:
				counter = 0
			if counter == 4:
				return True
		counter = 0
	return False

# this checks the diagonal win / It is split into 2 pieces, the lower half and the upper half
def check_win_diagonal_a(player):
	counter = 0
	for y in range(3, 5+1):
		x = 6
		while y > -1:
			if game[x][y] == player:
				counter += 1
			else:
				counter = 0
			if counter == 4:
				return True
			y -= 1
			x -= 1
		counter = 0
	return False

def check_win_diagonal_b(player):
	counter = 0
	stop = -1
	for x in range(5, 3-1, -1):
		y = 5
		while y > stop:
			if game[x][y] == player:
				counter += 1
			else:
				counter = 0
			if counter == 4:
				return True
			y -= 1
			x -= 1
		counter = 0
		stop += 1
	return False

# this checks the diagonal win \ It is split into 2 pieces, the lower half and the upper half
def check_win_diagonal_c(player):
	counter = 0
	for y in range(3, 5+1):
		x = 0
		while y > -1:
			if game[x][y] == player:
				counter += 1
			else:
				counter = 0
			if counter == 4:
				return True
			y -= 1
			x += 1
		counter = 0
	return False

def check_win_diagonal_d(player):
	counter = 0
	stop = -1
	for x in range(1, 3+1):
		y = 5
		while y > stop:
			if game[x][y] == player:
				counter += 1
			else:
				counter = 0
			if counter == 4:
				return True
			y -= 1
			x += 1
		counter = 0
		stop += 1
	return False

# checks whether it is a draw or not
def check_draw(board):
	counter = 0
	for col in range(7):
		for value in board[col]:
			if value >= 1:
				counter += 1
	if counter >= 42:
		return True
	else:
		return False

def check_win():
	result_player_1_vertical 	= check_win_vertical(1)
	result_player_1_horizontal 	= check_win_horizontal(1)

	result_player_2_vertical 	= check_win_vertical(2)
	result_player_2_horizontal 	= check_win_horizontal(2)

	result_player_1_diagonal_a 	= check_win_diagonal_a(1)
	result_player_1_diagonal_b	= check_win_diagonal_b(1)
	result_player_1_diagonal_c	= check_win_diagonal_c(1)
	result_player_1_diagonal_d	= check_win_diagonal_d(1)

	result_player_2_diagonal_a 	= check_win_diagonal_a(2)
	result_player_2_diagonal_b	= check_win_diagonal_b(2)
	result_player_2_diagonal_c	= check_win_diagonal_c(2)
	result_player_2_diagonal_d	= check_win_diagonal_d(2)

	if result_player_1_horizontal:
		return 1
	elif result_player_1_vertical:
		return 1
	elif result_player_2_horizontal:
		return 2
	elif result_player_2_vertical:
		return 2
	elif result_player_1_diagonal_a:
		return 1
	elif result_player_1_diagonal_b:
		return 1
	elif result_player_1_diagonal_c:
		return 1
	elif result_player_1_diagonal_d:
		return 1
	elif result_player_2_diagonal_a:
		return 2
	elif result_player_2_diagonal_b:
		return 2
	elif result_player_2_diagonal_c:
		return 2
	elif result_player_2_diagonal_d:
		return 2
	else:
		return False

def play_again():
	option = ''
	while True:
		option = input('Do you want to play again? [y/n]\n> ')
		if option.lower() == 'y':
			return True
			break
		elif option.lower() == 'n':
			return False
		else:
			message('Please choose [y/n]..')

# create player object
player1 = Player()
player2 = Player()

draw_board(game)
draw_column_numbers()

# MAIN GAME
while True:

	# collecting the names of the players
	while not names:
		player1.name = get_name('player 1')
		player2.name = get_name('player 2')
		message('This game is going to be played by ' + player1.name + ' and ' + player2.name + '.')
		names = True

	# player 1 makes a move
	if turn % 2 == 0:
		while True:
			try:
				player1.move = int(input(player1.name + ' [YELLOW]: Where do you want to place your piece? \n> '))
				if player1.move > 0 and player1.move <= 7:
					row_counter = row_full(player1.move)
				if player1.move < 1 or player1.move > 7:
					message('Choose from 1 to 7!')
				elif row_counter >= 6:
					message('This row is FULL, choose another row!')
				else:
					player1.move -= 1
					col_counter = 0
					for col in game[player1.move]:
						if col == 0:
							game[player1.move][col_counter] = 1
							break
						col_counter += 1
					turn += 1
					player1.move = 0
					break

			except ValueError:
				message('Please choose a number!')

	elif turn % 2 == 1:
		while True:
			try:
				player2.move = int(input(player2.name + ' [RED]: Where do you want to place your piece? \n> '))
				if player2.move > 0 and player2.move <= 7:
					row_counter = row_full(player2.move)
				if player2.move < 1 or player2.move > 7:
					message('Choose from 1 to 7!')
				elif row_counter >= 6:
					message('This row is FULL, choose another row!')
				else:
					player2.move -= 1
					col_counter = 0
					for col in game[player2.move]:
						if col == 0:
							game[player2.move][col_counter] = 2
							break
						col_counter += 1
					turn += 1
					player2.move = 0
					break

			except ValueError:
				message('Please choose a number!')

	# draw or update the game
	draw_board(game)

	# checks if it's a draw
	draw = check_draw(game)
	if draw:
		message('It\'s a draw!')
		option = play_again()
		if option:
			# reset the game
			game = make_game()
			draw_board(game)
			result = 0
		else:
			# terminate the game
			message('Come back later!')
			break

	# checks if someone won - win algoritm
	result = check_win()
	if result == 1:
		message(player1.name +' [YELLOW] WINS!!')
		option = play_again()
		if option:
			# reset the game
			game = make_game()
			draw_board(game)
			result = 0
		else:
			# terminate the game
			message('Come back later!')
			break

	elif result == 2:
		message(player2.name +' [RED] WINS!!')
		option = play_again()
		if option:
			# reset the game
			game = make_game()
			draw_board(game)
			result = 0
		else:
			# terminate the game
			message('Come back later!')
			break
